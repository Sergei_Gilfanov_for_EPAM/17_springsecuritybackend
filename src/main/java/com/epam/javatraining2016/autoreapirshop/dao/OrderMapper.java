package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
  void insert(OrderRow order);

  OrderRow selectShallow(int orderId);

  List<OrderRow> readList(OrderListRow orderList);

  List<OrderRow> readListPage(@Param("orderList") OrderListRow orderList,
      @Param("fromOrderId") int fromOrderId, @Param("pageLength") int pageLength,
      @Param("statusId") Integer statusId);

  List<OrderRow> readListFirstPage(@Param("orderList") OrderListRow orderList,
      @Param("pageLength") int pageLength, @Param("statusId") Integer statusId);

  OrderRow findPrevPage(@Param("orderList") OrderListRow orderList,
      @Param("fromOrderId") int fromOrderId, @Param("pageLength") int pageLength,
      @Param("statusId") Integer statusId);
}
