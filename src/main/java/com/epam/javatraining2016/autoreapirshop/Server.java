package com.epam.javatraining2016.autoreapirshop;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;
import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;

@SpringBootApplication
@EnableCaching
public class Server extends CachingConfigurerSupport {

  @Bean
  SessionFactory sessionFactory(@Value("${jdbc.url}") String url,
      @Value("${jdbc.user}") String username, @Value("${jdbc.password}") String password)
      throws IOException {
    return new SessionFactory(url, username, password);
  }

  @Bean
  AutoRepairShopServiceEndpoint autoRepairShopServiceEndpoint() {
    return new AutoRepairShopServiceEndpoint();
  }

  @Bean
  SimpleJaxWsServiceExporter simpleJaxWsServiceExporter() {
    SimpleJaxWsServiceExporter retval = new SimpleJaxWsServiceExporter();
    retval.setBaseAddress("http://localhost:8089/");
    return retval;
  }

  public static void main(String[] args) throws InterruptedException {
    SpringApplication application = new SpringApplication(Server.class);
    // Нам не нужен встроенный tomcat, SimpleJaxWsServiceExporter использует собственный http сервер
    application.setWebEnvironment(false);
    application.run(args);
  }

}
